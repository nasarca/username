create database IF NOT EXISTS username_db;

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(25) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into users (username) values('Isabella');
insert into users (username) values('Olivia');
--
-- Table structure for table `restricted_words`
--

CREATE TABLE IF NOT EXISTS `restricted_words` (
  `id` bigint(25) NOT NULL AUTO_INCREMENT,
  `word` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `word` (`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into restricted_words (word) values('cannabis');
insert into restricted_words (word) values('abuse');
insert into restricted_words (word) values('crack');
insert into restricted_words (word) values('damn');
insert into restricted_words (word) values('drunk');
insert into restricted_words (word) values('grass');

select case when count(1)>0 then 1 else 0 end  from restricted_words where 'abusedem' like concat('%', word, '%');