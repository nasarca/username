package com.wecode.username.controller;

import com.wecode.username.model.resources.UsernameResource;
import com.wecode.username.service.RestrictedWordService;
import com.wecode.username.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;


/**
 * Created by nasarca on 16/08/17.
 */

@SpringBootApplication
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RestrictedWordService restrictedWordService;

    @RequestMapping("/")
    public String index() {
        return "Hello to username App. Enjoy it!";
    }

    @RequestMapping(value = "/username/{username}", method = RequestMethod.GET)
    public @ResponseBody UsernameResource checkValidUsername(@PathVariable("username") String username){
        if(username.length() < 6){
            return new UsernameResource(false, "The username needs lest 6 characters");
        }
        return userService.checkUsername(username);
    }
}
