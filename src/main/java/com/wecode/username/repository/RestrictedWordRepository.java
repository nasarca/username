package com.wecode.username.repository;

import com.wecode.username.model.RestrictedWord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by nasarca on 18/08/17.
 */
public interface RestrictedWordRepository extends JpaRepository<RestrictedWord, Long>{
    @Query("select case when (count(rw) > 0) then true else false end from RestrictedWord rw where :word like CONCAT('%',rw.word,'%')")
    boolean contain(@Param("word") String word);
}
