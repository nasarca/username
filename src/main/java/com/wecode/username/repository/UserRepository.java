package com.wecode.username.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import com.wecode.username.model.User;
import org.springframework.stereotype.Repository;


/**
 * Created by nasarca on 16/08/17.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
     boolean existsByUsername(String username);
}