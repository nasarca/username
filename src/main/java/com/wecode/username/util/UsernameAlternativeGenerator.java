package com.wecode.username.util;

import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by nasarca on 18/08/17.
 */
@Component
public class UsernameAlternativeGenerator {
    private final String[] suggestWordAlternatives = { "elegant", "peaceful", "love", "cool", "nice", "sun", "moon", "crafty", "rock", "relax", "roots", "nature", "sweet", "calm", "smart", "intelligent", "cute", "diamond", "clever", "chill", "pretty", "astute", "quick", "cunning", "graceful"};

    public ArrayList<String> generateSuggestAlternatives(String username) {
        int i=0;
        String suggestWord;
        ArrayList<String> suggestAlternatives = new ArrayList<>();
        while (i<14){
            suggestWord = generateSuggestWord(username, i);
            if(!suggestAlternatives.contains(suggestWord)){
                i++;
                suggestAlternatives.add(suggestWord);
            }
        }
        return suggestAlternatives;
    }

    private String generateSuggestWord(String username, int i){
        int randomNumber = getRandomNumber();
        String suggestWord = suggestWord();
        if(i % 2 == 0) {
            suggestWord = (username + suggestWord + randomNumber);
        } else {
            suggestWord = (suggestWord + username + randomNumber);
        }
        return suggestWord;
    }

    public ArrayList<String> generateSuggestAlternatives() {

        return generateSuggestAlternatives(suggestWord());
    }

    private String suggestWord(){
        int randomSuggestWordIndex = getRandomNumberInRange(0, suggestWordAlternatives.length - 1);
        return suggestWordAlternatives[randomSuggestWordIndex];
    }

    private int getRandomNumber() {
        final int max = 9999;
        final int min = 1000;
        return getRandomNumberInRange(min, max);
    }

    private int getRandomNumberInRange(int min, int max) {
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
}
