package com.wecode.username.service;

import com.wecode.username.model.resources.UsernameResource;
import com.wecode.username.repository.UserRepository;
import com.wecode.username.util.UsernameAlternativeGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by nasarca on 17/08/17.
 */
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RestrictedWordService restrictedWordService;
    @Autowired
    private UsernameAlternativeGenerator usernameAlternativeGenerator;

    public UsernameResource checkUsername(String username) {
        if(restrictedWordService.isRestrictWord(username)) {
            return new UsernameResource("The username taken is a restricted word, please try another", suggestAlternativeWords());
        } else if(userRepository.existsByUsername(username)){
            return new UsernameResource("The username isn't available", suggestAlternativeWords(username));
        } else {
            return new UsernameResource(true, "The username is available");
        }
    }

    private ArrayList<String> suggestAlternativeWords(String username) {
        ArrayList<String> alternativeWords = usernameAlternativeGenerator.generateSuggestAlternatives(username);
        return alternativeWords;
    }

    private ArrayList<String> suggestAlternativeWords() {
        ArrayList<String> alternativeWords = usernameAlternativeGenerator.generateSuggestAlternatives();
        return alternativeWords;
    }
}
