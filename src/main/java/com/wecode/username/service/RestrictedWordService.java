package com.wecode.username.service;

import com.wecode.username.repository.RestrictedWordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by nasarca on 17/08/17.
 */
@Service
public class RestrictedWordService {
    @Autowired
    private RestrictedWordRepository restrictedWordRepository;

    public boolean isRestrictWord(String word) {
        return restrictedWordRepository.contain(word);
    }
}
