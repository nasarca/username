package com.wecode.username.model.resources;

import java.util.ArrayList;
import java.util.List;

public class UsernameResource extends BaseResource {

    private List<String> possibleWords = new ArrayList<>();

    public UsernameResource() {
        super();
    }

    public UsernameResource(boolean success, String message, List<String> possibleWords) {
        super(success, message);
        this.possibleWords = possibleWords;
    }
    public UsernameResource(boolean success, String message) {
        super(success, message);
        this.possibleWords = possibleWords;
    }

    public UsernameResource(String message, List<String> possibleWords) {
        super(message);
        this.possibleWords = possibleWords;
    }

    public List<String> getPossibleWords() {
        return possibleWords;
    }

    public void setPossibleWords(List<String> possibleWords) {
        this.possibleWords = possibleWords;
    }
}
