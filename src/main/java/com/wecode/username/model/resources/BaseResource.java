package com.wecode.username.model.resources;

/**
 * Created by nasarca on 16/08/17.
 */

public class BaseResource {

    private boolean success;
    private String message;

    public BaseResource() {
    }

    public BaseResource(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public BaseResource(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
