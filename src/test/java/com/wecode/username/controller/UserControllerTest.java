package com.wecode.username.controller;

import com.wecode.username.model.RestrictedWord;
import com.wecode.username.model.User;

import com.wecode.username.model.resources.UsernameResource;
import com.wecode.username.service.RestrictedWordService;
import com.wecode.username.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;


import java.util.ArrayList;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.mockito.BDDMockito.given;

/**
 * Created by nasarca on 20/08/17.
 */

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserService userServiceMock;

    @MockBean
    private RestrictedWordService restrictedWordServiceMock;

    private User olivia = new User("Olivia");

    @Before
    public void setUp() {
        RestrictedWord restrictedWord = new RestrictedWord("drunk");
        given(restrictedWordServiceMock.isRestrictWord(restrictedWord.getWord())).willReturn(true);
        UsernameResource usernameResource = new UsernameResource();
        usernameResource.setMessage("success");
        ArrayList<String> possibleWords = new ArrayList<>();
        possibleWords.add("loveolivia0101");
        possibleWords.add("oliviabeautiful0493");
        usernameResource.setPossibleWords(possibleWords);
        given(userServiceMock.checkUsername(olivia.getUsername())).willReturn(usernameResource);
    }

    @Test
    public void givenUsername_whenCheckUsername_thenReturnJson()
            throws Exception {

        MvcResult result = mvc.perform(get("/users/username/{username}", olivia.getUsername())
                .contentType(MediaType.APPLICATION_JSON)).andReturn();

    }
}
