# Username App for Intertec International

UsernameApp is a rest API for check if a username is taken or not. 

  - Send the username that you want to check if is available.
  - UsernameApp will suggest word if the username already exist.
  - UsernameApp will suggest word if the username is a restricted word.

### Tech

UsernameApp is a Java Project based on Spring Boot framework:

* [Spring Boot](https://projects.spring.io/spring-boot/) - Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run".
* [Docker](https://www.docker.com/) - Build, test, debug and deploy Linux and Windows Server container apps written in any programming language without risk of incompatibilities or version conflicts.
* [MySql](https://www.mysql.com/) - Open source relational database
* [Gradle](https://gradle.org/) - help to managed and build depedencies

### Installation

# Using Docker

Install Docker
Install Docker Compose
Build Docker image in the root 
```sh
$ cd YOUR_DOWNLOADED_FOLDER/username
$ docker build . -t java8
```
Check docker container to enter into mysql container. Need to be running docker containers.
```sh
$ docker ps
$ docker exec -ti YOUR_DOCKER_CONTAINER_ID bash
$ root@YOUR_DOCKER_CONTAINER_ID:/# mysql -uroot -p123yeah
mysql> -- copy and paste content into username/sql/username_db.sql
```

# Test and Examples

Please take a look this [Example Video](https://www.youtube.com/watch?v=Rm2rArFIPsA) and/or see the following examples.

localhost:8080/users/username/olivia
```sh
{"success":false,"message":"The username isn't available","possibleWords":["oliviaintelligent1240","quickolivia2026","oliviapretty9295","rockolivia2234","oliviadiamond5110","coololivia5320","oliviaintelligent1828","cleverolivia4134","oliviarock9097","cunningolivia2232","oliviacrafty5805","cuteolivia1841","oliviaastute3407","sweetolivia8977"]}
```
localhost:8080/users/username/abusedem
```sh
{"success":false,"message":"The username taken is a restricted word, please try another","possibleWords":["calmcool4872","mooncalm8121","calmcute5031","relaxcalm8746","calmquick2626","rootscalm2951","calmrock6905","gracefulcalm3604","calmcute4909","craftycalm5564","calmclever7838","gracefulcalm6717","calmsmart3930","intelligentcalm4077"]}
```
localhost:8080/users/username/iumari
```sh
{"success":true,"message":"The username is available","possibleWords":[]}
```